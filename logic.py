import math
from dateutil.relativedelta import relativedelta

import settings
from utils import now, next_weekday


def get_next_match_datetime(day=None, time=None):
    day = day or settings.MATCH_DAY
    time = time or settings.MATCH_TIME
    next_date = next_weekday(now(), day)
    hour, minute = time.split(':')
    return next_date.replace(hour=int(hour), minute=int(minute), second=0,
                             microsecond=0)


def get_time_left_until_next_match(dt=None):
    return relativedelta(dt or get_next_match_datetime(), now())


def calculate_price(count, price=None):
    price = price or settings.PRICE
    if count == 0:
        return price
    return math.ceil(price / count)
