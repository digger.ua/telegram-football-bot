import os
from dateutil import rrule

APP_ID = os.environ.get('APP_ID')
TELEGRAM_TOKEN = os.environ.get('TELEGRAM_TOKEN')
PORT = int(os.environ.get('PORT', '8443'))

MATCH_TIME = '11:00'
MATCH_DAY = rrule.SU.weekday  # Sunday
PRICE = 800
