MSG_HELLO = """
Привет {} ({}).
Организатор футбола в *{} на Кссрз* Николай приветсвует тебя!
Узнать че как /start /шо /че
Чтобы зарегаться /+
Чтобы привести с собой людей /+ X /+X
Чтобы слиться как лошок /-
Чтобы слить плюсов /- X /-X
Посмотреть список красавчиков /list /кто
Посмотреть количество красавчиков /count /сколько
Время до начала игры /time /когда /время
"""

MSG_LIST_PLAYERS = """
*Кто будет ({} {}):*
{}
{}

*Цена* {}. *На человека:* {}
"""

MSG_EXISTING = "Бля, не гони, {}. Ты уже в списке"

MSG_REGISTERED = "Красава {}!. Нас уже {}"
MSG_ADDS_REGISTERED = "Красава {}!. С твоими плюсами нас уже {}"

MSG_UNGREGISTERED = "Бляяяя... Нас теперь {} :("

MSG_TIME_LEFT = "Игра состоится *{} {}*. " \
                "До начала осталось *{} дней {} часов {} минут*"

MSG_COUNT = "Нас уже {}"

MSG_ADDS_INT_ERROR = "{}, не-не, ты число пиши"
MSG_ADDS_TOO_MUCH = "Ой бля, де ты столько людей возьмешь"

MSG_CONFIG_INVALID_CMD = "Я такое не умею"
MSG_CONFIG_INVALID_MATCH_ARGS = "Пиши в формате /config match день время. " \
                                "Например /config match воскресенье 12:00"

MSG_CONFIG_INVALID_PRICE_ARGS = "Пиши в формате /config price бабло. " \
                                "Например /config price 1000"

MSG_CONFIG_ERROR = "Чет не так, я хз"
MSG_CONFIG_MATCH_UPDATED = "Время матча изменено. Новое время {} {}"
MSG_CONFIG_PRICE_UPDATED = "Цена за игру изменена. Новая цена {}"


class Messages:
    HELLO = MSG_HELLO
    LIST_PLAYERS = MSG_LIST_PLAYERS
    EXISTING = MSG_EXISTING
    REGISTERED = MSG_REGISTERED
    UNREGISTERED = MSG_UNGREGISTERED
    TIME_LEFT = MSG_TIME_LEFT
    COUNT = MSG_COUNT

    ADDS_INT_ERROR = MSG_ADDS_INT_ERROR
    ADDS_REGISTERED = MSG_ADDS_REGISTERED
    ADDS_TOO_MUCH = MSG_ADDS_TOO_MUCH

    CONFIG_INVALID_CMD = MSG_CONFIG_INVALID_CMD
    CONFIG_INVALID_MATCH_ARGS = MSG_CONFIG_INVALID_MATCH_ARGS
    CONFIG_INVALID_PRICE_ARGS = MSG_CONFIG_INVALID_PRICE_ARGS

    CONFIG_ERROR = MSG_CONFIG_ERROR
    CONFIG_MATCH_UPDATED = MSG_CONFIG_MATCH_UPDATED
    CONFIG_PRICE_UPDATED = MSG_CONFIG_PRICE_UPDATED
