from datetime import timedelta, datetime

import pytz
from dateutil import rrule


def now():
    kiev = pytz.timezone('Europe/Kiev')
    return kiev.localize(datetime.now())


def yesterday():
    return now() - timedelta(days=1)


def next_weekday(dt, weekday):
    ahead = weekday - dt.weekday()
    if ahead < 0:
        ahead += 7
    return dt + timedelta(ahead)


def parse_day(day_str):
    if day_str.startswith('monday') or day_str.startswith('понедельник'):
        return rrule.MO.weekday
    elif day_str.startswith('tuesday') or day_str.startswith('вторник'):
        return rrule.TU.weekday
    elif day_str.startswith('wednesday') or day_str.startswith('среда'):
        return rrule.WE.weekday
    elif day_str.startswith('thursday') or day_str.startswith('четверг'):
        return rrule.TH.weekday
    elif day_str.startswith('friday') or day_str.startswith('пятница'):
        return rrule.FR.weekday
    elif day_str.startswith('saturday') or day_str.startswith('суббота'):
        return rrule.SA.weekday
    elif day_str.startswith('sunday') or day_str.startswith('воскресенье'):
        return rrule.SU.weekday
    else:
        raise Exception('Ты че мышь :/')


def day_num_to_human(day):
    if day == rrule.MO.weekday:
        return 'Понедельник'
    elif day == rrule.TU.weekday:
        return 'Вторник'
    elif day == rrule.WE.weekday:
        return 'Среда'
    elif day == rrule.TH.weekday:
        return 'Четверг'
    elif day == rrule.FR.weekday:
        return 'Пятница'
    elif day == rrule.SA.weekday:
        return 'Суббота'
    elif day == rrule.SU.weekday:
        return 'Воскресенье'
    else:
        raise Exception('Ты че мышь :/')
