import json
import logging

import redis

import settings
from logic import get_next_match_datetime, calculate_price
from utils import parse_day, day_num_to_human

logging.basicConfig(
    level=logging.DEBUG,
    format='%(asctime)s - %(name)s - %(levelname)s - %(message)s')

logger = logging.getLogger()
logger.setLevel(logging.INFO)


# todo: setex 2 weeks for matches

class Database:
    client = None  # type: redis.Redis

    def __init__(self, client):
        self.client = client


class ConfigDatabase(Database):
    MATCH_DATE_KEY = 'config:match_date'
    PRICE_KEY = 'config:price'

    def next_match_key(self):
        match_date = self.get_match_date()
        day = match_date['day']
        time = match_date['time']
        return str(int(get_next_match_datetime(day, time).timestamp()))

    def set_match_date(self, day_str, time_str):
        day = parse_day(day_str)

        hour, minute = time_str.split(':')

        hour = int(hour)
        minute = int(minute)

        if hour < 0 or hour > 23:
            raise Exception('ты бля')

        if minute < 0 or minute > 59:
            raise Exception('ты бля')

        config_obj = {'day': day, 'time': time_str}
        self.client.set(self.MATCH_DATE_KEY, json.dumps(config_obj))
        return True

    def get_match_date(self):
        obj = self.client.get(self.MATCH_DATE_KEY)
        obj = obj and json.loads(obj)

        match_day = obj and obj.get('day') or settings.MATCH_DAY
        match_time = obj and obj.get('time') or settings.MATCH_TIME

        obj = {'day': match_day, 'time': match_time}
        return obj

    def get_next_match_datetime(self):
        match_conf = self.get_match_date()

        day = day_num_to_human(match_conf['day'])

        return {
            'day': day,
            'time': match_conf['time'],
            'left': get_next_match_datetime(match_conf['day'],
                                            match_conf['time'])
        }

    def set_price(self, price):
        price = int(price)

        if price <= 0:
            raise Exception('ты бля')

        config_obj = {'price': price}
        self.client.set(self.PRICE_KEY, json.dumps(config_obj))
        return True

    def get_price(self):
        obj = self.client.get(self.PRICE_KEY)
        obj = obj and json.loads(obj)
        price = obj and obj.get('price') or settings.PRICE
        return price


class UserDatabase(ConfigDatabase):
    def key(self, user_id):
        return 'user:{}'.format(user_id)

    def add(self, user_id, full_name):
        # todo: setex
        return self.client.set(self.key(user_id), full_name)


class PlayerDatabase(ConfigDatabase):
    def key(self):
        return 'match:' + self.next_match_key()

    def fetch(self):
        return [int(p) for p in self.client.lrange(self.key(), 0, -1)]

    def register(self, user_id):
        # todo: setex
        return self.client.rpush(self.key(), user_id)

    def unregister(self, user_id):
        return self.client.lrem(self.key(), user_id)

    def count(self):
        return self.client.llen(self.key())


class AddsDatabase(ConfigDatabase):
    def key(self, user_id=''):
        return 'match:' + self.next_match_key() + ':' + str(user_id)

    def register(self, user_id, count):
        # todo: setex
        return self.client.set(self.key(user_id), count)

    def unregister(self, user_id, count):
        prev_count = self.count_by_user(user_id)
        logger.info(max(0, prev_count - count))
        logger.info(prev_count)
        logger.info(count)
        # todo: setex
        return self.client.set(self.key(user_id), max(0, prev_count - count))

    def count_by_user(self, user_id):
        return int(self.client.get(self.key(user_id)) or 0)

    def count(self):
        count = 0
        for full_key in self.client.keys(self.key() + '*'):
            key = full_key.decode('utf-8')
            user_id = int(key.split(':')[-1])
            count += self.count_by_user(user_id)
        return count

    def fetch(self):
        adds = []
        for full_key in self.client.keys(self.key() + '*'):
            key = full_key.decode('utf-8')
            user_id = int(key.split(':')[-1])
            adds.append(user_id)
        return adds
