import redis
import logging
import os
import functools
import locale

from utils import parse_day, day_num_to_human

locale.setlocale(locale.LC_ALL, 'uk_UA')

from telegram.ext import CommandHandler
from telegram.parsemode import ParseMode

from db import UserDatabase, PlayerDatabase, AddsDatabase, ConfigDatabase
from handlers import RegexpCommandHandler
from logic import get_time_left_until_next_match, \
    calculate_price
from messages import Messages

from settings import APP_ID, TELEGRAM_TOKEN, PORT

logging.basicConfig(
    level=logging.DEBUG,
    format='%(asctime)s - %(name)s - %(levelname)s - %(message)s')

logger = logging.getLogger()
logger.setLevel(logging.INFO)


class Core:
    def __init__(self, updater, dispatcher):
        self.updater = updater
        self.dispatcher = dispatcher

        self.client = redis.from_url(os.environ.get('REDIS_URL'))

        self.users = UserDatabase(self.client)
        self.config = ConfigDatabase(self.client)

        self.players = PlayerDatabase(self.client)
        self.adds = AddsDatabase(self.client)

        self.init_handlers()

    def init_hooks(func):
        @functools.wraps(func)
        def method(self, bot, update, *args, **kwargs):
            self.on_before_call(bot, update, *args, **kwargs)
            result = func(self, bot, update, *args, **kwargs)
            self.on_after_call(bot, update, *args, **kwargs)
            return result

        return method

    def on_before_call(self, bot, update, *args, **kwargs):
        self._save_user_to_db(bot, update)

    def on_after_call(self, bot, update, *args, **kwargs):
        pass

    def _save_user_to_db(self, bot, update):
        user = update.message.from_user
        self.users.add(user.id, user.full_name)
        logger.info('User "{}" "{}" has been added'.format(
            user.id, user.full_name))

    def init_handlers(self):

        _start = CommandHandler(('start', 'шо', 'че'), self._start)
        _register = CommandHandler('+', self._register, pass_args=True)
        _unregister = CommandHandler('-', self._unregister, pass_args=True)
        _multi_reg = RegexpCommandHandler(
            '\+([1-9]+)', self._multi_register, pass_groups=True)
        _multi_unreg = RegexpCommandHandler(
            '\-([1-9]+)', self._multi_unregister, pass_groups=True)

        _config = CommandHandler(('config', 'конфиг'), self._config,
                                 pass_args=True)

        _time = CommandHandler(('time', 'когда', 'время'), self._time)
        _list = CommandHandler(('list', 'кто'), self._list)
        _count = CommandHandler(('count', 'сколько'), self._count)

        self.dispatcher.add_handler(_start)
        self.dispatcher.add_handler(_register)
        self.dispatcher.add_handler(_multi_reg)
        self.dispatcher.add_handler(_unregister)
        self.dispatcher.add_handler(_multi_unreg)
        self.dispatcher.add_handler(_time)
        self.dispatcher.add_handler(_list)
        self.dispatcher.add_handler(_count)
        self.dispatcher.add_handler(_config)

    @init_hooks
    def _start(self, bot, update):
        user = update.message.from_user

        cfg = self.config.get_next_match_datetime()

        dt = cfg['left']

        date_str = dt.strftime("%A, %d %b %Y %H:%M:%S")
        date_str = date_str.capitalize()
        hello_message = Messages.HELLO.format(
            user.full_name, user.name, date_str).strip()
        update.message.reply_text(hello_message, parse_mode=ParseMode.MARKDOWN,
                                  quote=False)

    @init_hooks
    def _multi_register(self, bot, update, groups):
        try:
            count = int(groups[0])
        except Exception:
            msg = 'Ну нахуй ты так валишь..'
            update.message.reply_text(msg)
            return

        return self._register_adds_handler(bot, update, count)

    @init_hooks
    def _multi_unregister(self, bot, update, groups):
        try:
            count = int(groups[0])
        except Exception:
            msg = 'Ну нахуй ты так валишь..'
            update.message.reply_text(msg)
            return

        return self._unregister_adds_handler(bot, update, count)

    @init_hooks
    def _register_adds_handler(self, bot, update, count):
        user = update.message.from_user
        try:
            count = max(0, int(count))
            if count > 10:
                update.message.reply_text(Messages.ADDS_TOO_MUCH)
                return
            count = min(count, 10)
        except Exception:
            msg = Messages.ADDS_INT_ERROR.format(user.full_name)
            update.message.reply_text(msg, parse_mode=ParseMode.MARKDOWN)
            return

        self.adds.register(user.id, count)
        count = self.players.count() + self.adds.count()
        msg = Messages.REGISTERED.format(user.full_name, count)
        update.message.reply_text(msg, parse_mode=ParseMode.MARKDOWN)

    @init_hooks
    def _register(self, bot, update, args):
        if args:
            return self._register_adds_handler(bot, update, args[0])

        user = update.message.from_user
        existing = self.players.fetch()
        if user.id in existing:
            msg = Messages.EXISTING.format(user.full_name)
            update.message.reply_text(msg, parse_mode=ParseMode.MARKDOWN)
            return

        self.players.register(user.id)

        count = self.players.count() + self.adds.count()
        msg = Messages.REGISTERED.format(user.full_name, count)
        update.message.reply_text(msg, parse_mode=ParseMode.MARKDOWN)

    @init_hooks
    def _unregister_adds_handler(self, bot, update, count):
        user = update.message.from_user
        try:
            count = abs(int(count))
            count = max(0, count)
            if count > 10:
                update.message.reply_text(Messages.ADDS_TOO_MUCH)
                return
            count = min(count, 10)
        except ValueError:
            msg = Messages.ADDS_INT_ERROR.format(user.full_name)
            update.message.reply_text(msg, parse_mode=ParseMode.MARKDOWN)
            return

        self.adds.unregister(user.id, count)
        count = self.players.count() + self.adds.count()
        msg = Messages.UNREGISTERED.format(count)
        update.message.reply_text(msg, parse_mode=ParseMode.MARKDOWN)

    @init_hooks
    def _unregister(self, bot, update, args):
        if args:
            return self._unregister_adds_handler(bot, update, args[0])

        user = update.message.from_user
        self.players.unregister(user.id)
        count = self.players.count() + self.adds.count()
        msg = Messages.UNREGISTERED.format(count)
        update.message.reply_text(msg, parse_mode=ParseMode.MARKDOWN)

    @init_hooks
    def _time(self, bot, update):
        cfg = self.config.get_next_match_datetime()
        diff = get_time_left_until_next_match(cfg['left'])

        msg = Messages.TIME_LEFT.format(
            cfg['day'], cfg['time'],
            diff.days, diff.hours, diff.minutes)
        update.message.reply_text(
            msg, parse_mode=ParseMode.MARKDOWN, quote=False)

    @init_hooks
    def _list(self, bot, update):
        registered_players = self.players.fetch()
        registered_adds = self.adds.fetch()
        players = []
        for i, user_id in enumerate(registered_players, 1):
            try:
                member = bot.get_chat_member(update.message.chat_id, user_id)
            except Exception:
                full_name = user_id
            else:
                full_name = member.user.full_name
            players.append('{}. {}'.format(i, full_name))

        players = '\n'.join(players)

        adds_msgs = []
        count = len(registered_players)
        for user_id in registered_adds:
            try:
                member = bot.get_chat_member(update.message.chat_id, user_id)
            except Exception:
                full_name = user_id
            else:
                full_name = member.user.full_name
            for j in range(self.adds.count_by_user(user_id)):
                count += 1
                adds_msgs.append('{}. {} (+{})'.format(count, full_name, j + 1))

        adds_msg = '\n*Плюсы:*\n'
        adds_msg = adds_msg + '\n'.join(adds_msgs)
        total = len(registered_players) + self.adds.count()
        match_conf = self.config.get_match_date()

        price = self.config.get_price()
        per_player = calculate_price(total, price)

        msg = Messages.LIST_PLAYERS.format(
            day_num_to_human(match_conf['day']), match_conf['time'],
            players, adds_msg, price, per_player)

        update.message.reply_text(
            msg, parse_mode=ParseMode.MARKDOWN, quote=False)

    @init_hooks
    def _count(self, bot, update):
        count = self.players.count() + self.adds.count()
        msg = Messages.COUNT.format(count)
        update.message.reply_text(
            msg, parse_mode=ParseMode.MARKDOWN, quote=False)

    @init_hooks
    def _config(self, bot, update, args):
        user = update.message.from_user

        if not self.admin_check(user):
            update.message.reply_text('.!.', parse_mode=ParseMode.MARKDOWN)
            return

        try:
            config_type = args[0]
        except Exception:
            msg = Messages.CONFIG_INVALID_MATCH_ARGS
            update.message.reply_text(msg, parse_mode=ParseMode.MARKDOWN)
            return

        if config_type in ['match', 'game', 'игра', 'матч']:

            try:
                match_day = args[1]
                match_time = args[2]
            except Exception:
                msg = Messages.CONFIG_INVALID_MATCH_ARGS
                update.message.reply_text(msg, parse_mode=ParseMode.MARKDOWN)
                return

            try:
                self.config.set_match_date(match_day, match_time)
            except Exception as e:
                msg = Messages.CONFIG_ERROR
                update.message.reply_text(msg, parse_mode=ParseMode.MARKDOWN)
                return

            match_conf = self.config.get_match_date()

            msg = Messages.CONFIG_MATCH_UPDATED.format(
                day_num_to_human(match_conf['day']), match_conf['time']
            )
            update.message.reply_text(msg, parse_mode=ParseMode.MARKDOWN)

        elif config_type in ['price', 'цена']:

            try:
                price = args[1]
            except Exception:
                msg = Messages.CONFIG_INVALID_PRICE_ARGS
                update.message.reply_text(msg, parse_mode=ParseMode.MARKDOWN)
                return

            try:
                self.config.set_price(price)
            except Exception as e:
                msg = Messages.CONFIG_ERROR
                update.message.reply_text(msg, parse_mode=ParseMode.MARKDOWN)
                return

            msg = Messages.CONFIG_PRICE_UPDATED.format(
                price
            )
            update.message.reply_text(msg, parse_mode=ParseMode.MARKDOWN)

        else:
            msg = Messages.CONFIG_INVALID_CMD
            update.message.reply_text(msg, parse_mode=ParseMode.MARKDOWN)

    init_hooks = staticmethod(init_hooks)

    def admin_check(self, user):
        if user.full_name not in ['Igor Kolyadkin', 'Zrc', 'Myk']:
            return False

        return True

    def initialize(self):
        webhook_url = 'https://' + APP_ID + '.herokuapp.com/' + TELEGRAM_TOKEN
        self.updater.start_webhook(
            listen='0.0.0.0',
            port=PORT,
            url_path=TELEGRAM_TOKEN,
            webhook_url=webhook_url)
        self.updater.bot.set_webhook(webhook_url)
        self.updater.start_polling()

        self.updater.idle()
