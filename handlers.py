import re
import warnings
import logging

from telegram.ext import Handler
from telegram.update import Update

logging.basicConfig(
    level=logging.DEBUG,
    format='%(asctime)s - %(name)s - %(levelname)s - %(message)s')

logger = logging.getLogger()
logger.setLevel(logging.INFO)


class RegexpCommandHandler(Handler):
    def __init__(self,
                 command_regexp,
                 callback,
                 filters=None,
                 allow_edited=False,
                 pass_groups=False,
                 pass_groupdict=False,
                 pass_args=False,
                 pass_update_queue=False,
                 pass_job_queue=False,
                 pass_user_data=False,
                 pass_chat_data=False):
        super(RegexpCommandHandler, self).__init__(
            callback,
            pass_update_queue=pass_update_queue,
            pass_job_queue=pass_job_queue,
            pass_user_data=pass_user_data,
            pass_chat_data=pass_chat_data
        )

        self.command_regexp = command_regexp
        self.pass_groups = pass_groups
        self.pass_groupdict = pass_groupdict

        self.filters = filters
        self.allow_edited = allow_edited
        self.pass_args = pass_args

        # We put this up here instead of with the rest of checking code
        # in check_update since we don't wanna spam a ton
        if isinstance(self.filters, list):
            warnings.warn(
                'Using a list of filters in MessageHandler is getting '
                'deprecated, please use bitwise operators (& and |) '
                'instead. More info: https://git.io/vPTbc.')

    def check_update(self, update):
        if (isinstance(update, Update)
                and (
                        update.message or update.edited_message and self.allow_edited)):
            message = update.message or update.edited_message

            if message.text and message.text.startswith('/') and len(
                    message.text) > 1:
                command = message.text[1:].split(None, 1)[0].split('@')
                # in case the command was send without a username
                command.append(message.bot.username)

                if self.filters is None:
                    res = True
                elif isinstance(self.filters, list):
                    res = any(func(message) for func in self.filters)
                else:
                    res = self.filters(message)

                match = re.match(self.command_regexp, command[0])

                return res and (bool(match) and command[
                    1].lower() == message.bot.username.lower())
            else:
                return False

        else:
            return False

    def handle_update(self, update, dispatcher):
        optional_args = self.collect_optional_args(dispatcher, update)

        message = update.message or update.edited_message
        match = re.match(self.command_regexp, update.effective_message.text[1:])

        if self.pass_args:
            optional_args['args'] = message.text.split()[1:]

        if self.pass_groups:
            optional_args['groups'] = match.groups()
        if self.pass_groupdict:
            optional_args['groupdict'] = match.groupdict()

        return self.callback(dispatcher.bot, update, **optional_args)
